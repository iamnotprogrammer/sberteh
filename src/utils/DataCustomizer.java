package utils;

import data.*;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by user on 10.07.17.
 */
public class DataCustomizer {

    private DataCustomizer(){}

    public static InputData customizer(String name, String type, File file){

        if (type.equals(Constants.CLASS_FILE)){
            return new ClassFile(name, type, file);
        } else if(type.equals(Constants.DIRECTORY)){
            return new Directory(name, type, file);
        } else if(type.equals(Constants.JAR_FILE)){
            return new JarFile(name, type, file);
        } else if(type.equals(Constants.JAVA_FILE)){
            return new JavaFile(name, type, file);
        } else {
            Logger.getLogger(DataCustomizer.class.getName()).log(Level.WARNING, "Файл/директория не подходит" +
                    " под указанный тип.");
            return null;
        }
    }
}
