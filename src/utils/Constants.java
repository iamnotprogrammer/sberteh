package utils;

import java.nio.file.Paths;

/**
 * Created by user on 10.07.17.
 */
public class Constants {

    private Constants(){}

    public static final String WORK_DIR;
    public static final String JAR_FILE;
    public static final String JAVA_FILE;
    public static final String CLASS_FILE;
    public static final String DIRECTORY;

    static{
        WORK_DIR = Paths.get(System.getProperty("user.dir"), "classes").toString();
        JAR_FILE = "jar";
        JAVA_FILE = "java";
        CLASS_FILE = "class";
        DIRECTORY = "directory";
    }

}
