package data;

import utils.CustomClassLoader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Created by user on 10.07.17.
 */
public class ClassFile extends InputData {

    private CustomClassLoader customClassLoader = new CustomClassLoader(ClassFile.class.getClassLoader());

    public ClassFile(String name, String type, File file){
        this.setName(name);
        this.setType(type);
        this.setFile(file);
    }

    @Override
    public void load() {

        Class clazz = null;

        try {
            URL url = this.getFile().getParentFile().toURI().toURL();
            URL[] urls = new URL[]{url};
            URLClassLoader urlClassLoader = new URLClassLoader(urls);
            clazz = urlClassLoader.loadClass(this.getName().substring(0, this.getName().length() - 6));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (clazz == null){
            Logger.getLogger(ClassFile.class.getName()).log(Level.WARNING, "Класс не загрузился");
        } else {
            Logger.getLogger(ClassFile.class.getName()).info("Класс " + this.getName() + " успешно загрузился");
            setClazz(clazz);
        }
    }
//    public static void main(String args[]){
//
//        File file = new File("/home/user/JAVA/LinalProgramming/bin/TaskMain.class");
//        ClassFile classFile = new ClassFile(file.getName(),
//                file.getName().split("\\.")[file.getName().split("\\.").length-1],
//                file);
//        classFile.load();
//        Stream.of(classFile.getClazz().getDeclaredMethods())
//                .forEach(System.out::println);
//    }
}
