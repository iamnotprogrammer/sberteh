package data;

import java.io.File;

/**
 * Created by user on 10.07.17.
 */
public class JarFile extends InputData {

    public JarFile(String name, String type, File file) {
        this.setName(name);
        this.setType(type);
        this.setFile(file);
    }

    @Override
    public void load() {

    }
}
