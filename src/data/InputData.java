package data;

import java.io.File;

/**
 * Created by user on 10.07.17.
 */
public abstract class InputData {

    private String name;
    private String type;
    private File file;

    public Class getClazz() {
        return clazz;
    }

    protected void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    private Class clazz;

    public abstract void load();

    public File getFile() {
        return file;
    }

    protected void setFile(File file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    public String getType() { return type; }

    protected void setType(String type) {
        this.type = type;
    }
}
