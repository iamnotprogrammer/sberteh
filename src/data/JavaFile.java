package data;

import utils.CustomClassLoader;
import utils.СlassCompiler;

import java.io.File;

/**
 * Created by user on 10.07.17.
 */
public class JavaFile extends InputData {

    private СlassCompiler compiler;
    private CustomClassLoader customClassLoader = new CustomClassLoader(JavaFile.class.getClassLoader());

    public JavaFile(String name, String type, File file) {
        this.setName(name);
        this.setType(type);
        this.setFile(file);
    }

    @Override
    public void load() {
        compiler = new СlassCompiler(this.getFile());
        compiler.classCompile();

    }
}
